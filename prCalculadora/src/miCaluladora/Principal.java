package miCaluladora;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calculadora c = new Calculadora();
		
		System.out.println("Caluladora aritmetica");
		System.out.println("=====================");
		
		System.out.println("sumando 2 + 3 = "+c.suma(2, 3));
		System.out.println("restando 10 - 4 = "+c.resta(10, 4));
		System.out.println("multiplicando 2 * 3 = "+c.multiplica(2, 3));
		System.out.println("dividiendo 6 / 3 = "+c.divide(6, 3));
	}

}
