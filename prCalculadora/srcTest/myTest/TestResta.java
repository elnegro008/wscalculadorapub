package myTest;

import static org.junit.Assert.*;
import miCaluladora.Calculadora;

import org.junit.Before;
import org.junit.Test;

public class TestResta {
	Calculadora c;
	@Before
	public void setUp() throws Exception {
		c = new Calculadora();
	}

	@Test
	public void testResta() {
		assertTrue(c.resta(10,4) == 6);
	}

}
