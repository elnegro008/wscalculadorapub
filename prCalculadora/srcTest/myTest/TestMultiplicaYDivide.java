package myTest;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import miCaluladora.Calculadora;

import org.junit.Before;
import org.junit.Test;

public class TestMultiplicaYDivide {
	Calculadora c;

	@Before
	public void setUp() throws Exception {
		c = new Calculadora();
	}

	@Test
	public void testMultiplica() {
		//fail("Not yet implemented");
		assertTrue(c.multiplica(2,3) == 6);
	}

	@Test
	public void testDivide() {
		//fail("Not yet implemented");
		assertTrue(c.divide(6,3) == 2);
	}

}
