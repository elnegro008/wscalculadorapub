package myTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestMultiplicaYDivide.class, TestResta.class,
		TestSumaYResta.class })
public class AllTests {

}
