package myTest;

import static org.junit.Assert.*;
import miCaluladora.Calculadora;

import org.junit.Before;
import org.junit.Test;

public class TestSumaYResta {
	Calculadora c;

	@Before
	public void setUp() throws Exception {
		c = new Calculadora();
	}

	@Test
	public void testSuma() {
		//fail("Not yet implemented");
		assertTrue(c.suma(2,3) == 5);
	}
	
	@Test
	public void testResta() {
		//fail("Not yet implemented");
		assertTrue(c.resta(10,4) == 6);
	}

}
